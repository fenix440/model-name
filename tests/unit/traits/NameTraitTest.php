<?php

use Fenix440\Model\Name\Interfaces\NameAware;
use Fenix440\Model\Name\Traits\NameTrait;

/**
 * Class NameTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Name\Traits\NameTrait
 * Bartlomiej Szala <fenix440@gmail.com>
 */
class NameTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }


    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Name\Interfaces\NameAware
     */
    protected function getTraitMock()
    {
        $m = $this->getMockForTrait('Fenix440\Model\Name\Traits\NameTrait');
        return $m;
    }

    /**
     * Get a dummy object of that trait
     * @return DummyName
     */
    protected function getDummyObj(){
        return new DummyName();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/



    /**
     * @test
     * @covers  ::getDefaultName
     * @covers  ::setName
     * @covers  ::isNameValid
     * @covers  ::getName
     * @covers  ::hasName
     */
    public function getDefaultName()
    {
        $trait = $this->getTraitMock();
        $this->assertNull($trait->getName());
    }

    /**
     * @test
     * @covers  ::setName
     * @covers  ::getName
     * @covers  ::isNameValid
     * @covers  ::hasName
     * @covers  ::hasDefaultName
     */
    public function setAndGetName()
    {
        $trait = $this->getTraitMock();
        $name = "Wonderland";
        $trait->setName($name);

        $this->assertSame($name, $trait->getName());
    }

    /**
     * @test
     * @covers  ::setName
     * @covers  ::isNameValid
     * @expectedException \Fenix440\Model\Name\Exceptions\InvalidNameException
     */
    public function setInvalidName()
    {
        $trait = $this->getTraitMock();
        $name = (int)100000;

        $trait->setName($name);
    }

    /**
     * @test
     *
     * @covers ::setName
     * @covers ::getName
     * @covers ::isNameValid
     * @covers ::hasName
     * @covers ::hasDefaultName
     * @covers ::getDefaultName
     */
    public function getNameFromCustomDefault(){
        $dummy = $this->getDummyObj();
        $dummy->defaultName="Will Smith";

        $this->assertSame($dummy->defaultName, $dummy->getName());
    }


}


/**
 * Class DummyName
 *
 */
class DummyName
{

    use NameTrait;

    public $defaultName=null;

    public function getDefaultName(){
        return $this->defaultName;
    }
}
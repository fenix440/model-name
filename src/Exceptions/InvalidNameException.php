<?php namespace Fenix440\Model\Name\Exceptions;

/**
 * Invalid Name Exception
 *
 * Throws this exception when invalid Model name has been provided
 *
 * Bartlomiej Szala <fenix440@gmail.com>
 * @package Fenix440\Model\Name\Exceptions
 */
class InvalidNameException extends \InvalidArgumentException {

}
<?php namespace Fenix440\Model\Name\Interfaces;
use Fenix440\Model\Name\Exceptions\InvalidNameException;

/**
 * Interface NameAware
 *
 * A component/resource must be aware of "Name".
 * Provides an option to set, get and validate name for
 * given component.
 *
 * Furthermore, depending upon implementation, a default value might be returned, if no value has been set prior to obtaining it.
 *
 * Bartlomiej Szala <fenix440@gmail.com>
 * @package Fenix440\Model\Name\Interfaces
 */
interface NameAware {

    /**
     * Set name for given component
     * @param string $name          Name
     * @return void
     * @throws InvalidNameException If given name is invalid
     */
    public function setName($name);

    /**
     * Get name for given component
     *
     * @see NameAware::getDefaultName()
     * @see NameAware::setName($name)
     *
     * @return string|null
     */
    public function getName();

    /**
     * Validates if this component name is valid
     * @param string $name      Component name
     * @return bool             true/false
     */
    public function isNameValid($name);

    /**
     * Get this component default name
     * @return string|null
     */
    public function getDefaultName();

    /**
     * Check if this component has set a name
     * @return bool                     true/false
     */
    public function hasName();

    /**
     * Checks if this component has set a default name
     * @return bool                     true/false
     */
    public function hasDefaultName();


}
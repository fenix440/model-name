<?php namespace Fenix440\Model\Name\Traits;
use Fenix440\Model\Name\Exceptions\InvalidNameException;
use Aedart\Validate\String\NonEmptyStringValidator;

/**
 * Trait Name
 *
 * @see NameAware
 *
 * Bartlomiej Szala <fenix440@gmail.com>
 * @package Fenix440\Model\Name\Traits
 */
trait NameTrait {

    /**
     * Name for given component
     * @var null|string
     */
    protected $name=null;

    /**
     * Set name for given component
     * @param string $name          Name
     * @return void
     * @throws InvalidNameException If given name is invalid
     */
    public function setName($name){
        if(!$this->isNameValid($name))
            throw new InvalidNameException(sprintf('Name "%s" is invalid',$name));

        $this->name=(string)$name;
    }

    /**
     * Get name for given component
     *
     * @see NameAware::getDefaultName()
     * @see NameAware::setName($name)
     *
     * @return string|null
     */
    public function getName(){
        if(!$this->hasName() && $this->hasDefaultName()){
            $this->setName($this->getDefaultName());
        }
        return $this->name;
    }

    /**
     * Validates if this component name is valid
     * @param string $name      Component name
     * @return bool             true/false
     */
    public function isNameValid($name){
        return NonEmptyStringValidator::isValid($name);
    }

    /**
     * Get this component default name
     * @return string|null
     */
    public function getDefaultName(){
        return null;
    }

    /**
     * Check if this component has set a name
     * @return bool                     true/false
     */
    public function hasName(){
        if(!is_null($this->name))
            return true;
        return false;
    }

    /**
     * Checks if this component has set a default name
     * @return bool                     true/false
     */
    public function hasDefaultName(){
        return (!is_null($this->getDefaultName()))? true:false;
    }



}